// add new command to the existing Cypress interface
declare global {
  namespace Cypress {
    interface Chainable {
      login: typeof login;
    }
  }
}

export function login() {
  const testData = require('./testData.json')  
  cy.visit('/');
  cy.request({
    method: 'POST',
    url: '/login',
    form: true,
    body: {
      username: testData.email,
      password: testData.password,
    },
  });
}

Cypress.Commands.add('login', login);
