import Base from './base';
import PageLoader from './pageLoader';
import AnswerPage from '../pages/answerPage';

const routes = require('../../support/routes.json');

export default class AnswerPageLoader extends Base implements PageLoader {
  private path: string;

  constructor(
    path: string = `${routes.result}?input_s=5%20%2B%205&result=10`
  ) {
    super();
    this.path = path;
  }

  public load(): AnswerPage {
    cy.login();
    cy.visit(this.path);
    return new AnswerPage();
  }
}