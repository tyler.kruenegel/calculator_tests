import Page from '../pages/page';

export default interface PageLoader {
    load(): Page;
}