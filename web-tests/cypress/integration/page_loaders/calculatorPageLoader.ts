import { StatusCode } from './../../support/statusCode';
import PageLoader from './pageLoader';
import Base from './base';
import CalculatorPage from '../pages/calculatorPage';
import { HttpMethod } from '../../support/httpMethod';

const testData = require('../../support/testData.json')
const apiRoutes = require('../../support/apiRoutes.json');
const routes = require('../../support/routes.json');

export default class CalculatorPageLoader extends Base implements PageLoader {
    public load(): CalculatorPage {        
      cy.route(HttpMethod.POST, apiRoutes.calculate, 'fx:calculate/calculatorResponse.json').as('successResponse');
        cy.route({
          method: HttpMethod.POST,
          url: apiRoutes.logOut,
          status: StatusCode.NoContent,
        }).as("logOutResponse");
        cy.login();
        cy.visit(routes.calculator);

        return new CalculatorPage();
    }
}