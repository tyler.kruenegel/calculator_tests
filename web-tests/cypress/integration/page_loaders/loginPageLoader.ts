import LoginPage from '../pages/loginPage';
import Base from './base';
import PageLoader from './pageLoader';

const routes = require('../../support/routes.json')

export default class LoginPageLoader extends Base implements PageLoader {

    public load(): LoginPage {
        cy.visit(routes.root);
        return new LoginPage();
    }
}