import LoginPageLoader from './page_loaders/loginPageLoader';

const testData = require('../support/testData.json');

describe('Login', () => {
    let loginPage: any;

    beforeEach(() => {
        loginPage = new LoginPageLoader().load();
    });

    it('successfully logs in', () => {
        loginPage.submitCompleteLoginForm(testData.email, testData.password);
        cy.location().its('pathname').should('eq', '/calculator');
    });

    // Only checking that fields are required here.  No need to test that HTML5 is doing what it should be. 
    describe('Form inputs', () => {
        it("email is required", () => {
            loginPage.emailInput.should('have.attr', 'required');
        });

        it('password is required', () => {
            loginPage.passwordInput.should('have.attr', 'required');
        });
    });

    describe('Errors', () => {
        it("shows error when authentication fails", () => {
            loginPage.submitCompleteLoginForm('wrong@wrong.com', 'emailandpass');
            loginPage.errorMessage.should('contain', 'Please check your login details and try again.')
        });
    });
});