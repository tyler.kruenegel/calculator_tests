import Page from "./page";

const selectors = require("./selectors/answerPage.json");

export default class AnswerPage implements Page {
    public get inputDisplay(): Cypress.Chainable<JQuery<any>> {
        return cy.get(selectors.inputDisplay);
    }

    public get answer(): Cypress.Chainable<JQuery<any>> {
        return cy.get(selectors.answer);
    }

    public clickLogout(): void {
        cy.get(selectors.logoutButton).click();
    }

    public clickPerformNewCalculation(): void {
        cy.get(selectors.newCalculationButton).click();
    }
}