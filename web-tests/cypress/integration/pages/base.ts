import { StatusCode } from './../../support/statusCode';
import { HttpMethod } from '../../support/httpMethod';

export default class Base {
  protected fixturesRoute: any;
  protected selectors: any;

  constructor(fixturesRoute: string, selectors: any) {
    this.fixturesRoute = fixturesRoute;
    this.selectors = selectors;
  }

  public get errorMessage(): Cypress.Chainable<JQuery<any>> {
    return cy.get(this.selectors.errorMessage);
  }

  public setErrorResponse(
    route: string,
    httpMethod: HttpMethod,
    statusCode: StatusCode,
    error: string
  ) {
    cy.route({
      method: httpMethod,
      response: `fx:${this.fixturesRoute}_${error}`,
      url: route,
      status: statusCode,
    }).as("errorResponse");
  }

  public delayResponse(route: string, httpMethod: HttpMethod, delay: number) {
    cy.route({ method: httpMethod, url: route, response: {}, delay: delay }).as("delayedResponse");
  }
}