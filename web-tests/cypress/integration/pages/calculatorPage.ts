import Page from './page';
import Base from './base'

const selectors = require('./selectors/calculatorPage.json');

export default class CalculatorPage extends Base implements Page {
  constructor() {
    super("calculate/calculatorResponse", selectors);
  }

  public enterCalculation(input: string) {
    cy.get(selectors.calculationField).type(input).blur();
  }

  public submitCalculation() {
    cy.get(selectors.calculateButton).click();
  }

  public clickLogout() {
    cy.get(selectors.logoutButton).click();
  }
}