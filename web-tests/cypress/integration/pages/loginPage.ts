import Page from './page';
import Base from './base';

const selectors = require('./selectors/loginPage.json');

export default class LoginPage extends Base implements Page {
  constructor() {
    super('login/loginResponse', selectors);
  }

  public get submitButton(): Cypress.Chainable<JQuery<any>> {
    return cy.get(selectors.loginButton);
  }

  public get emailInput(): Cypress.Chainable<JQuery<any>> {
    return cy.get(selectors.emailField);
  }

  public get passwordInput(): Cypress.Chainable<JQuery<any>> {
    return cy.get(selectors.passwordField);
  }

  public enterEmail(email: string): void {
    this.emailInput.type(email);
  }

  public enterPassword(password: string): void {
    this.passwordInput.type(password);
  }

  public submitCompleteLoginForm(email: string, password: string): void {
      this.enterEmail(email);
      this.enterPassword(password);
      this.submit();
  }

  public submit(): void {
    this.submitButton.click();
  }
}