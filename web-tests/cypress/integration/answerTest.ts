import AnswerPageLoader from './page_loaders/answerPageLoader';

const routes = require('../support/routes.json');

describe('Answer Page', () => {
  it('displays the answer', () => {
    const answerPage = new AnswerPageLoader().load();
    answerPage.answer.should('have.attr', 'placeholder', '10');
  });

  it('returns to calculator page', () => {
      const answerPage = new AnswerPageLoader(
        `${routes.result}?input_s=5%20%2B%205&result=0`
      ).load();
      answerPage.clickPerformNewCalculation();
      cy.location().its('pathname').should('eq', routes.calculator)
  });

  it('logs out', () => {
    const answerPage = new AnswerPageLoader().load();
    answerPage.clickLogout();
    cy.location().its('pathname').should('eq', routes.login)
  });
});
