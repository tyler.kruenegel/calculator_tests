const routes = require('../support/routes.json');

describe('Authentication', () => {
  it('redirects to login page if no token', () => {
    cy.visit(routes.calculator);
    cy.location().its('pathname').should('eq', '/login');
  });
});
