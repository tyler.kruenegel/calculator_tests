import { StatusCode } from './../support/statusCode';
import CalculatorPageLoader from './page_loaders/calculatorPageLoader';
import { HttpMethod } from '../support/httpMethod';

const apiRoutes = require('../support/apiRoutes.json');
const routes = require('../support/routes.json');

describe('Calculator', () => {
  let calculatorPage: any;

  beforeEach(() => {
    calculatorPage = new CalculatorPageLoader().load();
  });

  it('submits valid calculation to the server', () => {
    calculatorPage.enterCalculation('5 + 5');
    calculatorPage.submitCalculation();
    cy.wait('@successResponse')
      .its('requestBody')
      .then((requestBody: any) => {
        expect(requestBody.input).to.eq('5 + 5');
      });
    cy.location().then(location =>  {
      expect(location.pathname).to.eq(routes.result);
      expect(location.search).to.eq('?input_s=5%20%2B%205&result=12')
    });
  });

  it('logs out', () => {
    calculatorPage.clickLogout();
    cy.location().its('pathname').should('eq', routes.login)
  });

  describe('Hanldes Server Errors', () => {
    it('shows error when Input not valid', () => {
      calculatorPage.setErrorResponse(apiRoutes.calculate, HttpMethod.POST, StatusCode.BadRequest, 'invalidChar');  
      // The calculation submitted does not matter here as we are setting the server response. We just need to pass the local regex.  
      calculatorPage.enterCalculation('5+5');
      calculatorPage.submitCalculation();
      calculatorPage.errorMessage.should(
          'have.text',
          'Input not valid. See example below.'
      );
    });

    it('shows correct error when unexpectedError', () => {
      calculatorPage.setErrorResponse(
        apiRoutes.calculate,
        HttpMethod.POST,
        StatusCode.InternalServerError,
        'unexpectedError'
      );
      calculatorPage.enterCalculation('5+5');
      calculatorPage.submitCalculation();
      calculatorPage.errorMessage.should(
        'have.text',
        "We've experienced an unexpected error.  Please try again later or contact support."
      );
    });

    it('returns to calculator when going back from 500 page', () => {
      calculatorPage.setErrorResponse(
        apiRoutes.calculate,
        HttpMethod.POST,
        StatusCode.InternalServerError,
        'unexpectedError'
      );
      calculatorPage.enterCalculation('5+5');
      calculatorPage.submitCalculation();
      cy.go('back');
      cy.location().its('pathname').should('eq', routes.calculator)
    })

    it('shows timeout error when server does not respond', () => {
      calculatorPage.delayResponse(apiRoutes.calculate, HttpMethod.POST, 20000);
      calculatorPage.enterCalculation('5 + 5');
      calculatorPage.submitCalculation();
      cy.wait('@delayedResponse')
      calculatorPage.errorMessage.should(
        'have.text',
        'We are experience an issue contact our server.  Please try again later or contact support'
      );
    });
  });
});
