# Web Tests

Example project using a modified form of Page Object Model and Cypress as the driver.  One cool thing I was thinking as building this demo is that this is a perfect usecase for TDD with Cypress when the server development is lagging behind front end.  API contracts could be specified then Web development started using these tests without an available server.

## Why Cypress
For me I was choosing between Cypress and Playwright.  I am still getting familiar with Playwright's syntax as I've only spent a few hours working with it to this point.  For the sake of development speed I chose Cypress.  

In a production project on an team using Microsoft tooling I would probably choose playwright depending on the requirements.  Playwright supports one additional browser, has easier support for mobile webviews and I've found runtime to be better on playwright.


## Architecture


#### Simple Rules to follow

1. Tests should only call a single Page Loader.
2. No Page Loader should know about another Page Loader.
3. Page Loaders should only have 1 public method.  Load()
    1. Load() will do everything needed to put the application on that page.  It may need to load some state into localstorage, set a react state directly in the case of a react app, or set default mock server responses.  
    2. Page Loader is the only place multiple pages can be called at once.  That being said it should be avoided at all costs and state should be created without the use of the UI. 
4. No Page should know about another page. 
5. Each page has a single `selectors` json file where all selectors are stored.


## Test Coverage

### Why not talk to the server? 
By writing the tests in a way that both check outgoing requests as well as how the application handles incoming responses we test a full server integration with only 1 piece removed. (The network).

We are asserting that the applications outgoing request matches what the server expects.  

We are asserting that the application is handling all known server responses.  This includes both positive and negative responses.  We are actually digging even deeper to test negative responses.  Often in a web application all possible error responses cannot be tested because it is "not possible" for the application to end up in that state.  As we know what can go wrong eventually will so we want to be sure if its an available server response the application is handling it.

#### But wait what if the server changes it's API contract? 
The API tests are ensuring that if a breaking change happens on the server it is not unintentional.  API Tests responsible for catching Server Bugs,  WebTests responsible for catching Web Bugs.

This is the one place in the current architecture that some responsibility gets put on the developers to make sure they are not changing API contracts.  

There are solutions to this problem though if Swagger or OpenApi is being used they support default responses.  We can generate a typescript client and against the specification and then load our default responses from that specification!  So as the API contract changes our default server also change!  Now we get the full benefit of removing networking and fast mocks without sacraficing "real" server responses. 


## Usage
Update `cypress/support/testData.json` to set the password to a valid password.
Execute `yarn install`
Execute `yarn test`.

In a real world environment and with more time I would dockerize the test excecution.  By dockerizing cypress we can enable parallel docker runs without using the cypress dashboard. 

## Additional Tests

This test suite is meant to model how I would approach automation for this application.  It does not encompass testing of all the features.  But lays out the architecture and approach.  To complete the front end testing of this application I would add tests to cover the following scenarios.

### Calculator Answer Page
- Proper display of the answer on the answer page. 
    - Long Answer over 50 numbers.
    - Short Answer.
    - Answer with decimals. 
- Returning to the input page
    - Check that input is empty when returned.
- Logout
    - Check that user is sent to Log in page.
    - Verify that token cookie is infact cleared.    
