using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace AcceptanceTests.Api.RestWrapper
{
    public class RestResponse<T> : RestResponse
    {
        public RestResponse(HttpResponseMessage response)
            : base(response)
        {
        }

        public T Data => JsonConvert.DeserializeObject<T>(this.Content);
        
        public new RestResponse<T> Validate(HttpStatusCode expectedStatusCode = 0, bool shouldValidate = true)
        {
            return (RestResponse<T>)base.Validate(expectedStatusCode, shouldValidate);
        }
    }
}