using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using AcceptanceTests.Context;
using Newtonsoft.Json;

namespace AcceptanceTests.Api.RestWrapper
{
    public class RestRequest
    {
        private readonly TestContext _testContext;
        private readonly Dictionary<string, string> _headers = new Dictionary<string, string>();
        private readonly Dictionary<string, string> _parameters = new Dictionary<string, string>();
        private HttpContent _content = new ByteArrayContent(new byte[0]);

        public RestRequest(string endpoint, TestContext testContext)
        {
            _testContext = testContext;
            this.Endpoint = endpoint;
        }

        public string ContentText { get; private set; }

        public string Endpoint { get; private set; }

        public HttpMethod Method { get; private set; }

        public Uri Uri { get; private set; }

        public TimeSpan RequestTime { get; set; }

        public HttpRequestMessage BuildRequestMessage(HttpClient client)
        {
            this.ApplyHeaders(client);
            var uriString = $"{client.BaseAddress}{this.Endpoint}";

            this.Uri = new Uri(uriString);
            var request = new HttpRequestMessage { Method = this.Method, RequestUri = this.Uri };
            this.AddContentToMessage(request);
            return request;
        }

        public RestRequest Authorize(string email, string password)
        {
            var bytes = Encoding.ASCII.GetBytes($"{email}:{password}");
            Header("Authorization", $"Basic {Convert.ToBase64String(bytes)}");
            return this;
        }

        public RestRequest Post()
        {
            this.Method = HttpMethod.Post;
            return this;
        }

        public RestRequest Get()
        {
            this.Method = HttpMethod.Get;
            return this;
        }

        public RestRequest Header(string name, string value)
        {
            _headers[name] = value;
            return this;
        }

        public RestRequest Body(object content)
        {
            ContentText = JsonConvert.SerializeObject(content, Formatting.Indented);
            _content = new StringContent(ContentText);
            return this;
        }
        
        public RestRequest AddFormParameter(string paramName, string paramValue)
        {
            this._parameters.Add(paramName, paramValue);
            return this;
        }

        public RestRequest AsJson()
        {
            Header("Content-Type", @"application/json;charset=UTF-8");
            return this;
        }

        public RestRequest AsForm()
        {
            Header("Content-Type", @"application/x-www-form-urlencoded");
            _content = new FormUrlEncodedContent(_parameters);
            return this;
        }

        public RestRequest Authorize(string authToken)
        {
            
            Header("Authorization", $"bearer {authToken}");
            return this;
        }

        private static MultipartFormDataContent AddMultipartSection(MultipartFormDataContent content, HttpContent section)
        {
            content.Add(section);
            return content;
        }

        private void AddContentToMessage(HttpRequestMessage request)
        {
            if (this.Method != HttpMethod.Get)
            {
                request.Content = _content;
            }
        }

        private void ApplyHeaders(HttpClient client)
        {
            foreach (var header in _headers)
            {
                try
                {
                    if ("content-type".Equals(header.Key.ToLower()))
                    {
                        _content.Headers.ContentType = MediaTypeHeaderValue.Parse(header.Value);
                    }
                    else
                    {
                        client.DefaultRequestHeaders.Add(header.Key, header.Value);
                    }
                }
                catch (Exception e)
                {
                    throw new Exception($"Failed to add header: {header.Key}  with value: {header.Value}", e);
                }
            }
        }
    }
}