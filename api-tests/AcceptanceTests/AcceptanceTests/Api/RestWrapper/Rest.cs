using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using AcceptanceTests.Context;
using AcceptanceTests.Utilities;

namespace AcceptanceTests.Api.RestWrapper
{
    public class Rest
    {
        private readonly TestContext _testContext;
        private readonly AppSettings _appSettings;

        public Rest(TestContext testContext, AppSettings appSettings)
        {
            _testContext = testContext;
            _appSettings = appSettings;
        }
        
        public RestRequest VersionedRequest(string endpoint, string apiVersion = null)
        {
            apiVersion ??= _appSettings.ApiVersion;
            var path = $"api/{apiVersion}/{endpoint}";
            return new RestRequest(path, _testContext);
        }

        public RestRequest Request(string endpoint)
        {
            return new RestRequest(endpoint, _testContext);
        }
        
        public async Task<RestResponse> ExecuteAsync(RestRequest request, HttpClient client = null)
        {
            client ??= Client();
            var requestMessage = request.BuildRequestMessage(client);
            var stopwatch = Stopwatch.StartNew();

            var responseMessage = await client.SendAsync(requestMessage).ConfigureAwait(false);
            stopwatch.Stop();
            request.RequestTime = stopwatch.Elapsed;
            var response = new RestResponse(responseMessage);
                
            _testContext.LastRequest = request;
            _testContext.RequestMade(request);
            _testContext.LastResponse = response;

            return response;
        }
        
        public async Task<RestResponse<T>> ExecuteAsync<T>(RestRequest request, HttpClient client = null)
            where T : new()
        {
            client ??= Client();
            var requestMessage = request.BuildRequestMessage(client);
            var stopwatch = Stopwatch.StartNew();

            var responseMessage = await client.SendAsync(requestMessage).ConfigureAwait(false);
            stopwatch.Stop();
            request.RequestTime = stopwatch.Elapsed; 
            var response = new RestResponse<T>(responseMessage);
            _testContext.LastRequest = request;
            _testContext.RequestMade(request);
            _testContext.LastResponse = response;

            return response;
        }
        
        private HttpClient Client() => new HttpClient
        {
            BaseAddress = new Uri(_appSettings.Host),
            Timeout = TimeSpan.FromMinutes(2)
        };
    }
}