using System;
using Newtonsoft.Json;

namespace AcceptanceTests.Api.RestWrapper
{
    public class RequestException
    {
        [JsonProperty("error_message")]
        public string ErrorMessage { get; set; }
        public Exception InnerException { get; set; }
    }
}