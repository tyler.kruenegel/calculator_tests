using System;
using Newtonsoft.Json;

namespace AcceptanceTests.Api.RestWrapper
{
    public static class ExceptionMapper
    {
        public static RequestException DeserializeException(string content)
        {
            RequestException exception;
            try
            {
                exception = JsonConvert.DeserializeObject<RequestException>(content);
            }
            catch (Exception e)
            {
                exception = new RequestException()
                {
                    ErrorMessage = "Unknown",
                    InnerException = e
                };
            }

            return exception;
        }
    }
}