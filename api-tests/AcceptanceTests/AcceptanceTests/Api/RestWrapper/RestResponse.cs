using System.IO;
using System.Net;
using System.Net.Http;

namespace AcceptanceTests.Api.RestWrapper
{
        public class RestResponse
    {
        public RestResponse(HttpResponseMessage response)
        {
            Response = response;
            Content = response?.Content?.ReadAsStringAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }
        public HttpStatusCode StatusCode => Response.StatusCode;
        
        public string Content { get; }

        public HttpResponseMessage Response { get; }
        
        public RestResponse Validate(HttpStatusCode expectedStatusCode = 0, bool shouldValidate = true)
        {
            if (shouldValidate)
            {
                if (expectedStatusCode != StatusCode)
                {
                    if (expectedStatusCode == 0 && (int)this.StatusCode >= 200 && (int)this.StatusCode < 300)
                    {
                        return this;
                    }

                    throw new ResponseValidationException(GetException(), this.BuildErrorMessage());
                }
            }
            return this;
        }
        
        public RequestException GetException()
        {
            return ExceptionMapper.DeserializeException(Content);
        }

        private string BuildErrorMessage()
        {
            var url = "unknown";

            if (Response.RequestMessage != null)
            {
                url = WebUtility.UrlDecode(Response.RequestMessage.RequestUri.AbsoluteUri);
            }

            return $"Bad status code {Response.StatusCode} For request: [{Response.RequestMessage.Method.ToString()}] {url}\nContent: {Content}";
        }
    }
}