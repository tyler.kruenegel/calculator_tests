using System;

namespace AcceptanceTests.Api.RestWrapper
{
    public class ResponseValidationException : Exception
    {
        public ResponseValidationException(RequestException requestException, string requestUri)
            : base($"{requestUri}: {requestException?.ErrorMessage}")
        {
            this.RequestException = requestException;
        }

        public RequestException RequestException { get; }
    }
}