using System.ComponentModel.DataAnnotations;

namespace AcceptanceTests.Api.Calculator
{
    public class CalculatorResponse
    {
        [Required]
        public string Result { get; set; }
    }
}