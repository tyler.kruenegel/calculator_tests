using System.Threading.Tasks;
using AcceptanceTests.Api.RestWrapper;
using AcceptanceTests.Utilities;
using NUnit.Framework;

namespace AcceptanceTests.Api.Calculator
{
    public class CalculatorApi
    {
        private readonly Rest _rest;

        public CalculatorApi(Rest rest)
        {
            _rest = rest;
        }

        public Task<RestResponse<CalculatorResponse>> CalculateAsync(CalculatorRequest requestBody, Account account)
        {
            var request = _rest.VersionedRequest("calculator").Authorize(account.Email, account.Password).Body(requestBody).AsJson().Post();

            return _rest.ExecuteAsync<CalculatorResponse>(request);
        }
    }
}