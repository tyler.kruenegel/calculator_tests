using Newtonsoft.Json;

namespace AcceptanceTests.Api.Calculator
{
    public class CalculatorRequest
    {
        [JsonProperty("input")]
        public string Input { get; set; }
    }
}