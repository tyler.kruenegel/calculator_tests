using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AcceptanceTests.Api.Calculator;
using AcceptanceTests.Api.RestWrapper;

namespace AcceptanceTests.Context
{
    public class TestContext
    {
        private readonly Stack<RestRequest> _requests;

        public TestContext()
        {
            _requests = new Stack<RestRequest>();
        }
        
        public string CurrentAuthToken { get; set; }
        
        public string Email { get; set; }
        
        public string Password { get; set; }
        
        public CalculatorResponse CalculatorResponse { get; set; }
        
        public RestResponse LastResponse { get; set; }

        public RestRequest LastRequest { get; set; }
        
        public void RequestMade(RestRequest request) =>
            _requests.Push(request);
        
        public List<RestRequest> AllRequests() =>
            _requests.ToList();
        
        public object GetValue(string propertyName)
        {
            PropertyInfo property = GetType().GetProperty(propertyName);
            return !(property == null) ? property.GetValue(this) : throw new ArgumentException("Property '" + propertyName + "' doesn't exist on the test context");
        }
    }
}