using System.Threading;

namespace AcceptanceTests.Utilities
{
    public class AppSettings
    {
        public const string SectionName = "Appsettings";
        public string Host { get; set; }
        
        public string ApiVersion { get; set; }
        
        public Account Account { get; set; }
        
    }
    
    // Treating the Account object as temporary so placing inside AppSettings.cs for simplicity. 
    public class Account
    {
        public string Email { get; set; }
            
        public string Password { get; set; }
    }  
}