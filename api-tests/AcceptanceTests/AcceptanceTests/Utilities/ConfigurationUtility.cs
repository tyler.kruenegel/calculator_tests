using System;
using System.Collections.Immutable;
using System.Configuration;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace AcceptanceTests.Utilities
{
    public class ConfigurationUtility
    {
        private readonly IConfigurationRoot configRoot;

        static ConfigurationUtility()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory))
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.personal.json")
                .AddEnvironmentVariables()
                .Build();

            Current = new ConfigurationUtility(configuration);
        }

        private ConfigurationUtility(IConfigurationRoot root)
        {
            this.configRoot = root;
        }
        
        public static ConfigurationUtility Current { get; }

        public T GetSection<T>(string sectionKey)
        {
            return this.configRoot.GetSection(sectionKey).Get<T>();
        }
    }
}