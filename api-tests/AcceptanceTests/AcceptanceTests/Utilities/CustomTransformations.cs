using System;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Utilities
{
    [Binding]
    public class CustomTransformations
    {
        [StepArgumentTransformation(@"(try to |)")]
        public bool ValidateTryTo(string tryTo)
        {
            return !"try to ".Equals(tryTo, StringComparison.Ordinal);
        }

        [StepArgumentTransformation]
        public string TransformLineBreak(string input)
        {

            return input.Replace("{nl}", "\n"); 
        }
    }
}