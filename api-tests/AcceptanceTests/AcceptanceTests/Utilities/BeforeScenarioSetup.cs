using System;
using System.IO;
using AcceptanceTests.Context;
using BoDi;
using Microsoft.Extensions.Configuration;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Utilities
{
    [Binding]
    public class BeforeScenarioSetup
    {
        private readonly IObjectContainer _objectContainer;
        private AppSettings _appSettings;

        public BeforeScenarioSetup(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }
        
        [BeforeScenario()]
        public void InitializeTestContext()
        {
            _objectContainer.RegisterInstanceAs(new TestContext());
        }

        [BeforeScenario()]
        public void LoadSettings()
        {
            _appSettings = ConfigurationUtility.Current.GetSection<AppSettings>(AppSettings.SectionName);
            _objectContainer.RegisterInstanceAs<AppSettings>(_appSettings);
        }
    }
}