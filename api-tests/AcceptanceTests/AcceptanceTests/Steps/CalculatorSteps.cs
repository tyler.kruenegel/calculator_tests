using System.Net;
using System.Threading.Tasks;
using AcceptanceTests.Api.Calculator;
using AcceptanceTests.Context;
using AcceptanceTests.Utilities;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        private readonly TestContext _testContext;
        private readonly CalculatorApi _calculatorApi;
        private readonly AppSettings _appSettings;

        public CalculatorSteps(TestContext testContext, CalculatorApi calculatorApi, AppSettings appSettings)
        {
            _testContext = testContext;
            _calculatorApi = calculatorApi;
            _appSettings = appSettings;
        }
        
        [Given(@"I (try to |)submit '(.*)' for a calculation")]
        public async Task WhenITryToSubmitForACalculationAsync(bool shouldValidate, string input)
        {
            await this.SubmitCalculationAsync(input, _appSettings.Account, shouldValidate);
        }

        [Then(@"the answer is '(.*)'")]
        public void ThenTheAnswerIs(string answer)
        {
            _testContext.CalculatorResponse.Result.Should().Be(answer);
        }

        private async Task SubmitCalculationAsync(string input, Account account, bool shouldValidate = true)
        {
            var calculateRequest = new CalculatorRequest
            {
                Input = input
            };
            var response = await _calculatorApi.CalculateAsync(calculateRequest, account).ConfigureAwait(false);

            _testContext.CalculatorResponse = response.Validate(HttpStatusCode.OK, shouldValidate).Data;
        }
    }
}