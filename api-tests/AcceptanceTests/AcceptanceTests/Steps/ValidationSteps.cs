using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Reflection;
using AcceptanceTests.Context;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class ValidationSteps
    {
        private readonly TestContext _testContext;

        public ValidationSteps(TestContext testContext)
        {
            _testContext = testContext;
        }
        
        [Then(@"the '(.*)' contains the required fields")]
        public void ThenTheContainsTheRequiredFields(string propertyName)
        {
            var responseObject = _testContext.GetValue(propertyName);
            var missingFields = FindMissingFields(responseObject, responseObject.GetType());
            missingFields.Should().NotBeEmpty("The following fields are missing", missingFields);
        }

        [Then(@"the status code is '(.*)'")]
        public void ThenTheStatusCodeIs(HttpStatusCode statusCode)
        {
            _testContext.LastResponse.StatusCode.Should().Be(statusCode);
        }
        
        [Then(@"the error is '(.*)' with a '(.*)' status code")]
        public void ThenTheExceptionTypeIsWithAStatusCode(string errorType, HttpStatusCode statusCode)
        {
            var actualException = _testContext.LastResponse.GetException();

            actualException.Should().NotBeNull("An error should have been thrown but the call was successful.");
            actualException.ErrorMessage.Should().Be(errorType);
            ThenTheStatusCodeIs(statusCode);
        }
        
        private IEnumerable<string> FindMissingFields<T>(T responseObject, Type type, string parentTypeName = null)
        {
            List<string> missingFields = new List<string>();
            parentTypeName = parentTypeName ?? type.Name;
            var properties = type.GetProperties().Where(prop => prop.IsDefined(typeof(RequiredAttribute), true) || prop.IsDefined(typeof(System.ComponentModel.DataAnnotations.RequiredAttribute), true));

            foreach (var property in properties)
            {
                var qaRequired = property.GetCustomAttributes(typeof(RequiredAttribute)).OfType<RequiredAttribute>().ToList();

                var value = property.GetValue(responseObject);
                if (value == null)
                {
                    missingFields.Add($"{parentTypeName}.{property.Name}");
                }
                else
                {
                    if (property.PropertyType == typeof(string))
                    {
                        if (string.IsNullOrEmpty((string)value))
                        {
                            missingFields.Add($"{parentTypeName}.{property.Name}");
                        }
                    }
                    else if (typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
                    {
                        IEnumerable enumerable = (IEnumerable)value;
                        var genericType = enumerable.GetType().GetTypeInfo().GenericTypeArguments[0];
                        int index = 0;
                        foreach (var child in enumerable)
                        {
                            missingFields.AddRange(this.FindMissingFields(child, genericType, $"{parentTypeName}.{property.Name}[{index++}]"));
                        }
                    }
                    else
                    {
                        missingFields.AddRange(this.FindMissingFields(value, property.PropertyType, $"{parentTypeName}.{property.Name}"));
                    }
                }
            }

            return missingFields;
        }
    }
}