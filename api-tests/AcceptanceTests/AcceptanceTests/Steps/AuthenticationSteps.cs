using AcceptanceTests.Utilities;
using TechTalk.SpecFlow;

namespace AcceptanceTests.Steps
{
    [Binding]
    public class AuthenticationSteps
    {
        private readonly AppSettings _appsettings;
        public AuthenticationSteps(AppSettings appSettings)
        {
            _appsettings = appSettings;
        }

        [Given(@"I have invalid credentials")]
        public void GivenIHaveInvalidCredentials()
        {
            _appsettings.Account.Email = "invalid";
            _appsettings.Account.Password = "wrong";
        }
    }
}