Feature: Calculate
  
  Scenario Outline: When a user submits a valid calculation they recieve the correct answer
    Given I submit '<input>' for a calculation
    Then the answer is '<answer>'
    Examples: 
    | input                                         | answer                        | #Comment                                                 |
    | 5 + 5                                         | 10                            | Basic addition                                           |
    | 5 - 5                                         | 0                             | Basic subtraction                                        |
    | 5 * 5                                         | 25                            | Basic multiplication                                     |
    | 25 / 5                                        | 5                             | Basic division                                           |
    | 15 + -9                                       | 6                             | Addition of negative number                              |
    | 6 + 3 * 8                                     | 30                            | Order of operation multiplication before addition        | 
    | 6 - 2 + 4 / 5 * 9                             | 11.19999999999999928945726424 | Order of operation multiplication and division           |
    | 7 - 0 * 0 + 4 * 0 + 11                        | 18                            | 0 multiplaction while accounting for order of operations |
    | 8 * 40 - 9 * 55 / 2 * 345 + 40 + 90 * 300     | -58027.5                      | Complex order of operations resulting in negative answer |
    | -123934 * 546 + 23923 * 23932 + -239323       | 504617949                     | Complex order of operations with + negative number       |
    | 3.4329 * 23.32 * 43                           | 3442.374804                   | Calculation with decimals                                |
    | 3.4 / 2.39                                    | 1.422594142259414225941422594 | Dividing decimals                                        |
    | 3 + 02                                        | 5                             | Calculation where a number has a leading 0               |