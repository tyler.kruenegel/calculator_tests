Feature: CalculateError

  Scenario Outline: When a user submits an invalid calculation the recieve the correct error
    Given I try to submit '<input>' for a calculation
    Then the error is '<errorType>' with a '<statusCode>' status code
    Examples:
      | input    | errorType         | statusCode | #Comment                                                                                      |
      | c3 + 8   | Input not valid.  | BadRequest | letter                                                                                        |
      | 8 ! 9    | Input not valid.  | BadRequest | ! symbol                                                                                      |
      | 9e2 + 6  | Input not valid.  | BadRequest | Euhlers number for the purposes of this test I'm going to assume euhlers nubmers are invalid. |
      | 8 ^ 8    | Input not valid.  | BadRequest | ^ symbol                                                                                      |
      | 8 + 9 *  | Input not valid.  | BadRequest | Calculation left open at end.                                                                 |
      | / 8 - 9  | Input not valid.  | BadRequest | Calculation left open at beginning.                                                           |
      | 9 * / 8  | Input not valid.  | BadRequest | Operators placed beside each other.                                                           |
      | 9 {nl}   | Input not valid.  | BadRequest | Line break                                                                                    |

  Scenario: When a user tries to perform a calculation with no auth they get a 401
    Given I have invalid credentials
    And I try to submit '5+5' for a calculation
    Then the error is 'Unauthorized access' with a 'Unauthorized' status code