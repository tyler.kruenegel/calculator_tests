# API Tests

Example tests built using speclfow and System.Net.Http to interact with the API. 

## Architecture
I acknowledge that for a simple calculator app that just performs calculations that the Architecture is a little to verbose.  But I'm imagining and planning for a future where the application can possible do more than just simple calculations.  Maybe the calculations get saved for the user to access later or more. 

### Rest Wrapper
I am wrapping System.Net.Http for 2 reasons rather than just use it out of the box.  Because System.Net.Http is a networking library we may want to change it in the future for something faster and better? By limiting the scope of files it touches I can easily swap to another framework if needed.  

The second reason is it gives me the ability to back in some defaults I later don't need to work about.  Things like saving every request and response to test context to be accessed later.  Exception handling also gets built into the RestWrapper. 

`Validate()` allows us to check the expected status code of a response before moving on.  Anytime we call an API and we expect a positive response we `Validate`.  This prevents an API from "silent" failing in the test then having the test fail farther down the line due to some missing state. 

### Test Context
This is going to be our Test state each scenario gets a new TestContext and it is thread safe when running test in parallel. 

### API
Api objects have a 1 to 1 relationship with the controllers on the server.  No Api object should have any knowledge of a Step object or another Api object.

### Steps
Step object are going to be our implementation of the Api.  Without exception no Step class should initialize more than 1 Api Object.  If multiple Api's are needed in a Step file then the Step file is doing to much.  In the case of needing to create state across many Api controllers a coordinator object would be the solution.  A coordinator uses the step classes to build state in simple clean steps.  You will not see any coordinators in this demo because it was such a simple app there was no need. 
  
### What else can be done? 
- Add a custom transformation to transform `try to` in the step definition names to a `bool` argument for `shouldValidate`. This would eliminate the reduntant step definitions I created for happy and sad paths. 

- If Swagger or OpenApi is used a generated client could be used eliminating a bunch of the Rest boiler plate as well as the need to manually create Http Models. The generated client can still be piped to send responses into TestContext.  I've done this before with generated Nswag clients. 

- Logging.  Obviously logging is missing from these tests.  Anytime you have stateful integration tests logging is key when you run into issues. Logging can be added into the Rest framework. 

- Performance monitoring or at least performance logging.  We are collecting response times for each request.  We can log this or save them to a performance file that Developers can look at or at a very least we save them as an artifact in VSTS to have some historical view of API performance over time.  

## Usage
Install the dotnet cli

The password and email used for the tests can be overridden using env variables.
`CALCULATOR__AppSettings_Account_Email` and `CALCULATOR__AppSettings_Account_Password`

Execute:
1. `dotnet build`
2. `dotnet test`
