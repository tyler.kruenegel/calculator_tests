# Calculator Testing

## Executing the tests

### Web Tests
`cypress/support/testData.json` will need to be updated with a valid password before execution.  On a live project I would build and execute the tests against a local instance so the password would just be checked into source code. 

Execute `docker-compose run cypress` from the root directory for this project to run the UI tests. 

### Api Tests
The password and email used for the tests can be overridden using env variables.
`Appsettings__Account__Email` and `Appsettings__Account__Password`

Execute:
1. `dotnet build`
2. `dotnet test`

### Docker
Both projects are also set up to run in docker.  At the root you can execute `docker-compose up` to run both sets of tests or `docker-compose run cypress` and `docker-compose run api-test` to run each suite individually. 

## Automation Strategy
Automation would be divided into 2 parts.  API Tests and Web Tests.  The business logic is going to tested entirely through the API Tests. Web Tests will not be excercising the server.  Between the 2 sets of tests an entire end to end suite of tests is created with networking skipped!

### Web Tests
Written in Cypress and located in the `web-tests` directory.  See the README.md in that directory for a deep dive into the architecture and thought behind the tests. 

### API Tests
Written using specflow and located in the `api-tests` directory.  See the README.md in that directory for a deep dive into the architecture and thought behind the tests.

### When Tests Would Run
Because external networking has been removed from tests they execute fast!  This allows us to easily run these in CI as part of PR and Merge validation.  I've found that being proactive with test execution leads to the most stable and useful tests.  So rather than waiting for code to deploy to execute tests we run when PR's are created so developers know if they are introducing a breaking change before merging. 

Faster more stable tests also result in the tests being run more often during local development. 

## Assumption
Since the requirements specify only a login page.  I am going to work under the assumption that this is a piece of an existing application where user creation is present somewhere else.  I'm going to assume no user creation is available on the login page.  If user creation is possible there are more scenarios and complexity that would need to be tested.  

## Questions

- The requirements call out the 4 basic operands as allowed inputs but nothing else.  Are we going to be supporting paranthesis for order of operations control? 
- What are the response time performance requirements for the calculate api? 
- Is there an upper end limit for the number of operations that could be entered in one input?
- Is there a limit on how long a token can last? 
- Limits on stale session before timeout? 
- Will be accepting the `e` numeric constant?
- Should `÷` be accepted for division?
- How will extremely long numbers be out put? Euhlers number?

## Manual Testing
For this particular app the bulk of my testing effort would be automation.  We can cover all of the scenarios quickly and indefinitely into the future with automated API tests. 

That being said as part of validating the app I would be executing both the happy path and as many error paths as possible.  This will ensure that the front end and back end are both on the same page when it comes to API contracts.  

I would also be talking to the web and server developers to ensure they are all on the same page.  I am always surprised how often that communication line breaks down.

# Bugs

### UI
- When using the browser back button from the 500 page the user is returned to the login page.
- Visiting the login page while logged in does not redirect to the calculator.
- After login content is being requested from http://qechallenge2-mcg.azurewebsites.net/calculator which technically works because it redirects back to https but it would still be better to not have that happening. 
- No Http Timeout set on calls to calculator API leading to a seemingly unresponsive Ui when the server is not responding. 

### API
- Calculator API returns a 500 when a number has a leading 0.
- Calculator API returns a 500 when a line break is in the request.
- Calculator 403 error does not use the property `error_message` and instead returns the field `error`. 
